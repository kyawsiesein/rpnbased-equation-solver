﻿using System;
using System.Collections.Generic;

namespace EquationSolver {
	class Program {
		public Dictionary<string, int> operatorPrecedence = new Dictionary<string, int> ();

		public Program() {
			this.operatorPrecedence.Add ("+", 0);
			this.operatorPrecedence.Add ("-", 0);
			this.operatorPrecedence.Add ("*", 1);
			this.operatorPrecedence.Add ("/", 1);
			this.operatorPrecedence.Add ("%", 1);
			this.operatorPrecedence.Add ("^", 1);
		}

		static void Main (string [] args)
		{
			string userEquation = Console.ReadLine ();
			string [] splittedEquation = userEquation.Split (" ");
			Console.WriteLine (splittedEquation [0]);
			if (splittedEquation [0] == "calc") {
				string lhs = manipulateEquation (splittedEquation);
				Program program = new Program ();
				program.convertToRpn (lhs);
			} else {
				Console.WriteLine ("Your equation must start with calc");
			}
		}

		// ---------------------------------------
		// string manipulation
		// ---------------------------------------
		static string manipulateEquation (string [] userEquation)
		{
			List<string> equation = new List<string> ();
			for (int i = 1; i < userEquation.Length; i++) {
				string character = userEquation [i];
				if (character == "=") {
					break;
				}
				equation.Add (character);
			}
			return String.Join("",equation);
		}

		// ---------------------------------------
		// convert to rpn
		// ---------------------------------------
		public void convertToRpn (string equationString) {
			Stack<string> operatorStack = new Stack<string>();
			List<string> output = new List<string>();

			char [] equation = equationString.ToCharArray ();
			for (int i = 0; i < equation.Length; i++) {
				Console.WriteLine ("here");

				string character = equation [i].ToString ();


				// it is an operator 
				if (this.operatorPrecedence.ContainsKey (character)) {
					// check if there are any operators in the stack
					if (operatorStack.Count > 0) {
						string lastOperator = operatorStack.Peek ();
						int loVal = this.operatorPrecedence [lastOperator];
						int coVal = this.operatorPrecedence [character];
						if (coVal > loVal) {
							Console.WriteLine ("I am greater than you. LOL!");
							operatorStack.Push (character);
						} else if (coVal <= loVal) {
							// remove last operator from operator stack
							operatorStack.Pop ();
							// add last operator to output
							output.Add (lastOperator);
							// add current operator to operator stack
							operatorStack.Push (character);
						}
					} else {
						operatorStack.Push (character);
					}
				} else {
					if (character == "x") {
						// it is an x
						continue;
					} else {
						// it is not an operator and not x
						output.Add (character);
					}
				} // end of character checking

				while (i == equation.Length && operatorStack.Count > 0) {
					output.Add (operatorStack.Peek ());
					operatorStack.Pop ();
				}

			} // end of for loop



			foreach(string s in output.ToArray ()) {
				Console.WriteLine (s);
			}
		}
	}
}