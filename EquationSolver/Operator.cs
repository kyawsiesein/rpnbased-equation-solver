﻿using System;
namespace EquationSolver
{
    public class Operator
    {
		public string operatorSign { get; set; }
		public int operatorAssociativity { get; set; }
        public int operatorPrecedence { get; set; }
	}
}
